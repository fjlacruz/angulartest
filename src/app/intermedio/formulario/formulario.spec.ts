import { Formularioregister } from "./formulario";
import { FormBuilder } from '@angular/forms';

describe('Pruebas de Formularios.....', () => {
    let componente: Formularioregister;
    beforeEach(() => {
        componente = new Formularioregister(new FormBuilder());
    });

    it('Deber crear un form con dos campos', () => {
        expect(componente.form.contains('email')).toBeTruthy();
        expect(componente.form.contains('password')).toBeTruthy();
    });

    it('El email debe ser obligatorio', () => {
        const control = componente.form.get('email');
        control.setValue('');
        expect(control.valid).toBeFalsy();
    });

    it('El email debe ser valido', () => {
        const control = componente.form.get('email');
        control.setValue('idsistemas15@gmail.com');
        expect(control.valid).toBeTruthy();
    });

});