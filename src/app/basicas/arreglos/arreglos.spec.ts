import { obtenerRobots } from "./arreglos";


describe('Pruebas de arreglos', () => {
    it('Deber retornar al menos tres objetos', () => {
        const resp = obtenerRobots()
        expect(resp.length).toBeGreaterThanOrEqual(3);
    });
    it('Debe existir xxx', () => {
        const resp = obtenerRobots()
        expect(resp).toContain('xxx');
    })
});