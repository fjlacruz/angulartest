import { incremetar } from "./numeros";

describe('Pruebas de numeros', () => {
    it('Deber retornar 100, si el numero ingresado es mayor a 100', () => {
        const resp = incremetar(300)
        expect(resp).toBe(100);
    });
    it('Deber retornar el numero ingresado + 1, si no es mayor a 100', () => {
        const resp = incremetar(50)
        expect(resp).toBe(51);
    })
});