

//describe('Pruebas de string');
//it('Debe retornar un string');

import { mensaje } from "./string";


describe('Pruebas de string', () => {
    it('Deber regresa un string', () => {

        const resp = mensaje('Javier');
        expect(typeof resp).toBe('string');

    })
});

describe('PDebe retornar un saludo con el nombre enviado', () => {
    it('Deber regresa un string', () => {

        const nombre = 'Juan'
        const resp = mensaje(nombre);
        expect(resp).toContain('Saludos ' + nombre);

    })
});
