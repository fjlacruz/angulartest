import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';

import { RouterMedicoComponent } from './router-medico.component'; { }
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/observable/empty';


class FakeRouter {
  navigate(params) { }
}

class FakeActivateRote {
  //params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push(valor) {
    this.subject.next(valor);
  }

  get params() {
    return this.subject.asObservable();
  }

}


describe('RouterMedicoComponent', () => {
  let component: RouterMedicoComponent;
  let fixture: ComponentFixture<RouterMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RouterMedicoComponent],
      providers: [{ provide: Router, useClass: FakeRouter }, { provide: ActivatedRoute, useClass: FakeActivateRote }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Debe redireccionar a medico cuando se guarde', () => {
    const router = TestBed.get(Router);
    const spy = spyOn(router, 'navigate');
    component.guardarMedico();
    expect(spy).toHaveBeenCalledWith(['medico', '123']);
  });

  it('Debe colocar el id=nuevo', () => {
    component = fixture.componentInstance;

    const activateROute: FakeActivateRote = TestBed.get(ActivatedRoute);
    activateROute.push({ id: 'nuevo' });

    expect(component.id).toBe('nuevo');

  });


});
